const button = document.createElement('button');
button.innerText = 'Вычислить по IP';
document.body.append(button);
button.onclick = getIp;

async function getIp() {
    const getIP = await fetch('https://api.ipify.org/?format=json');
    let ip = await getIP.json();

    const dataIP = await fetch('http://ip-api.com/json/' + ip.ip);
    let data = await dataIP.json();

        let info = document.createElement('p');
        info.innerHTML = 'Your IP information';
        document.body.append(info);

        let props = ['country', 'regionName', 'city', 'zip'];
        props.forEach(function (item) {
            let itemInfo = document.createElement('p');
            document.body.append(itemInfo);
            itemInfo.innerHTML = `${item} : ${data[item]}`;
        })
}











// function buildDOM(data) {

//     let props = ['country', 'regionName', 'city', 'zip'];

//     props.forEach(function (item) {
//         let itemInfo = document.createElement('p');
//         document.body.append(itemInfo);
//         itemInfo.innerHTML = `${item} : ${data[item]}`;
//     })
// }

